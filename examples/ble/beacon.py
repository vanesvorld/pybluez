#!/usr/bin/env python3
"""PyBluez ble example beacon.py

Advertises a bluethooth low energy beacon for 15 seconds.
"""

import time

from bluetooth.ble import BeaconService

beacon_uuid="11111111-2222-3333-4444-555555555555"
service = BeaconService()

service.start_advertising(beacon_uuid,
                          1, 1, 1, 200)
print("beaconing with " + beacon_uuid + " for 150 seconds.")
time.sleep(150)
service.stop_advertising()

print("Done.")
